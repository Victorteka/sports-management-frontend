import React from 'react';
import { BrowserRouter as Router,Switch, Route} from 'react-router-dom'

import Home from './components/home/Home';
import Register from './components/register/Register'
import Login from './components/login/Login'
import SportsList from './components/sports/SportsList'
import Tournaments from './components/tournaments/Tournaments'
import LeagueList from './components/league/LeagueList';
import Logout from './components/logout/Logout';
import NewsList from './components/news/NewsList';
import LeagueDetails from './components/league/LeagueDetails';
import SportsDetails from './components/sports/SportsDetails';
import Profile from './components/profile/Profile';

function App() {
  return (
    <React.Fragment>
      <Router>
      <Switch>
        <Route exact path="/">
          <Home/>
        </Route>
        <Route exact path="/register">
          <Register/>
        </Route>
        <Route exact path="/login">
          <Login/>
        </Route>
        <Route exact path="/sports">
          <SportsList/>
        </Route>
        <Route path="/sports/:id">
          <SportsDetails />
        </Route>
        <Route exact path="/leagues">
          <LeagueList/>
        </Route>
        <Route path="/leagues/:id" >
          <LeagueDetails />
        </Route>
        <Route exact path="/news">
          <NewsList/>
        </Route>
        <Route exact path="/tournaments">
          <Tournaments/>
        </Route>
        <Route path="/profile">
          <Profile />
        </Route>
        <Route exact path="/logout">
          <Logout />
        </Route>
      </Switch>
      </Router>
    </React.Fragment>
  );
}

export default App;
