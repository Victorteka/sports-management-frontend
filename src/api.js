import axios from 'axios'

export default axios.create({
    baseURL: `https://sport-centre.herokuapp.com/api/v1/`
})