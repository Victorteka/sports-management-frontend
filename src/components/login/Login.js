import React, { useState } from 'react'
import { Link, useHistory } from 'react-router-dom'
import ChevronRightIcon from '@material-ui/icons/ChevronRight';

import API from '../../api'


function Login() {

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [isLoading, setIsLoading] = useState(false)

    const handleEmailChange = (event)=>{
        setEmail(event.target.value)
    }

    const handlePasswordChange = (event) =>{
        setPassword(event.target.value)
    }

    let history = useHistory()

    const loginUser = async () =>{
        const results = await API.post('auth/login', {
            "email": email,
            "password": password
        })
        console.log(results)
        localStorage.setItem('token', results.data.token)
        if (results.data.message === "Logged in successfully") {
            history.push('/')
            setIsLoading(false)
        }
        else {
            alert(results.data.message)
            setIsLoading(false)
        }
    }

    const handleSubmit = () =>{
        setIsLoading(true)
        loginUser()
    }

    return (
        <div className="bg-gray-300 h-screen flex justify-center">
            <div>
                <div className="max-w-md max-h-96 mt-28 bg-white rounded-md p-5">
                    <h1 className="text-2xl font-semibold text-center">
                        Login
                    </h1>
                    <input onChange={handleEmailChange} type="email" required placeholder="Enter email" 
                    className="min-w-full py-2 mt-2 px-3 border-2 border-gray-300 rounded focus:outline-none focus:border-blue-300"/>
                    <input onChange={handlePasswordChange} type="password" required placeholder="Enter password" 
                    className="min-w-full py-2 mt-2 px-3 border-2 border-gray-300 rounded focus:outline-none focus:border-blue-300"/>
                    {
                        isLoading ? <button onClick={handleSubmit} className="py-2 px-5 min-w-full font-semibold bg-gray-300 mt-5 rounded">
                                Loading ...
                            </button>:
                            <button onClick={handleSubmit} className="py-2 px-5 min-w-full font-semibold bg-red-500 mt-5 rounded text-white">
                                 Sign in
                            </button>
                    }
                </div>
                <Link to="/"><p className="mt-4 text-center">Login <ChevronRightIcon/> Home</p></Link>
            </div>
        </div>
    )
}

export default Login
