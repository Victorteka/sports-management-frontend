import React from 'react'
import Input from '@material-ui/core/Input'
import SearchIcon from '@material-ui/icons/Search'
import InputAdornment from '@material-ui/core/InputAdornment'

function SearchComponent() {
    return (
        <div className="pt-8 px-6 md:pl-96">
        <Input
        className=" max-w-full md:w-7/12"
        placeholder="Search for sport news ...."
          startAdornment={
            <InputAdornment position="start">
              <SearchIcon />
            </InputAdornment>
          }
        />
        </div>
    )
}

export default SearchComponent
