import React from 'react'
import CircularProgress from '@material-ui/core/CircularProgress'


function CircularProgressBar() {
    return (
        <div className="flex flex-col flex-wrap content-center">
            <CircularProgress className='mt-40 ml-8 mb-6' color="secondary" />
            <h1 className="text-2xl font-semibold">Loading ...</h1>
        </div>
    )
}

export default CircularProgressBar
