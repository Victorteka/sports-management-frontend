import React from 'react'
import Header from '../header/Header'

function Tournaments() {
    return (
        <React.Fragment>
            <Header/>
            <h1 className="text-3xl text-center pt-40 font-semibold">
                 No tournament going on at the momemt!
            </h1>
        </React.Fragment>
    )
}

export default Tournaments
