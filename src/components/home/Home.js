import React from 'react'
import { Link } from 'react-router-dom'
import Header from '../header/Header'

import './Home.css'
import Saf from '../../assets/images/saf.png'
import Kpl from '../../assets/images/kpl.png'
import Jkuat from '../../assets/images/jkuat.jpg'
import Airtel from '../../assets/images/airtel.png'
import Footer from '../footer/Footer'

function Home() {
    return (
        <React.Fragment>
            <Header/>
            <div>
                <div className="home__container">
                    <h1 className="text-3xl text-white font-bold text-center pt-40">
                        Sport centre.<br /> Home of sporting activities
                    </h1>
                    <div className="flex justify-center">
                    <Link to="/sports">
                        <button className="bg-red-400 hover:bg-red-500 mt-10 mb-8 text-white rounded-3xl px-8 py-3">
                            View Sports
                        </button>
                    </Link>
                    </div>
                </div>
            </div>
            <h1 className="text-2xl font-semibold text-center mt-16 mb-6">What we do</h1>
            <div className="flex flex-wrap justify-center">
                <div className="shadow-lg rounded p-4 w-1/4 mr-4">
                        <h1 className="text-xl font-bold text-center">Scout for talent</h1>
                </div>
                <div className=" w-1/4 shadow-lg rounded p-4 mr-4">
                        <h1 className="text-xl font-bold text-center">Mentor young players</h1>
                </div>
                <div className="max-w-md shadow-lg rounded p-4 w-1/4 mr-4">
                        <h1 className="text-xl font-bold text-center">Manage sporting activities</h1>
                </div>
            </div>
            <h1 className="text-2xl font-semibold text-center mt-16 mb-6">Our partners</h1>
            <div className="container flex flex-wrap justify-center">
                <img className="partners" src={Saf} alt="saf-logo"/>
                <img className="partners" src={Kpl} alt="saf-logo"/>
                <img className="partners" src={Jkuat} alt="saf-logo"/>
                <img className="airtel partners" src={Airtel} alt="saf-logo"/>
            </div>
            <Footer />
        </React.Fragment>
    )
}

export default Home
