import React from 'react'
import Header from '../header/Header'

function Logout() {

    localStorage.clear()

    return (
        <div>
            <Header />
            <h1 className="text-2xl font-semibold text-center pt-40">
                Logged out successfully <br/>
                Byeee!
            </h1>
        </div>
    )
}

export default Logout
