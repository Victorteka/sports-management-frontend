import React from 'react'
import './Footer.css'

function Footer() {
    return (
        <div className="footer__container">
            <p className="text-center pt-40"> Sport Centre &copy; 2021</p>
        </div>
    )
}

export default Footer
