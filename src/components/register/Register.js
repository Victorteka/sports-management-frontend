import React, { useState } from 'react'
import { Link, Redirect } from 'react-router-dom'
import ChevronRightIcon from '@material-ui/icons/ChevronRight';

import API from '../../api'

function Register() {

    const [username, setUsername] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [confirmPassword, setConfirmPassword] = useState('')
    const [redirect, setRedirect] = useState(null)

    const handleUsernameChange = (event) =>{
        setUsername(event.target.value)
    }

    const handleEmailChange = (event) =>{
        setEmail(event.target.value)
    }

    const handlePasswordChange = (event) =>{
        setPassword(event.target.value)
    }

    const handleConfirmPasswordChange = (event) =>{
        setConfirmPassword(event.target.value)
    }

    const handleSubmit =() =>{
        registerUser()
    }

    const registerUser = async () =>{
        if(password !== confirmPassword) {
            return alert('Incorrect passord!')
         }
        const results = await API.post('auth/register',{
            name: username,
            email: email,
            password: password
        })
        if(results.status === 200){
            setRedirect('/login')
        }else{
            alert(results.message.message)
        }
    }

    const redirectUser = ()=>{
        if(redirect !== null){
            return <Redirect to={redirect} />
        }
    }

    return (
        <div className="bg-gray-300 h-screen flex justify-center">
            {redirectUser()}
            <div>
                <div className="max-w-md max-h-96 mt-28 bg-white rounded-md p-5">
                    <h1 className="text-2xl font-semibold text-center">
                        Sign up
                    </h1>
                    <input onChange={handleUsernameChange} type="text" placeholder="Enter username" 
                    className="min-w-full py-2 mt-2 px-3 border-2 border-gray-300 rounded focus:outline-none focus:border-blue-300"/>
                    <input onChange={handleEmailChange} type="text" placeholder="Enter email" 
                    className="min-w-full py-2 mt-2 px-3 border-2 border-gray-300 rounded focus:outline-none focus:border-blue-300"/>
                    <input onChange={handlePasswordChange} type="password" placeholder="Enter password" 
                    className="min-w-full py-2 mt-2 px-3 border-2 border-gray-300 rounded focus:outline-none focus:border-blue-300"/>
                    <input onChange={handleConfirmPasswordChange} type="password" placeholder="Confirm password" 
                    className="min-w-full py-2 mt-2 px-3 border-2 border-gray-300 rounded focus:outline-none focus:border-blue-300"/>
                    <button onClick={handleSubmit}  className="py-2 px-5 min-w-full font-semibold bg-red-500 mt-5 rounded text-white">Sign up</button>
                </div>
                <Link to="/"><p className="mt-4 text-center">Sign up <ChevronRightIcon/> Home</p></Link>
            </div>
        </div>
    )
}

export default Register
