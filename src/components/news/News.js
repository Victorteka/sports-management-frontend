import React from 'react'
import ChevronRightIcon from '@material-ui/icons/ChevronRight';

function News({news}) {

    let imageUrls = []
    news.forEach(element => {
        if(element.multimedia.length !== 0){
            imageUrls.push(element.multimedia[0].url)
        }
    })

    const formateDate = (pubDate) =>{
        let date = new Date(pubDate)
        return date.toDateString()
    }

    return (
        <div className=" container mx-auto md:grid grid-cols-3 gap-4 pt-12" >
                {
                    news.map((singleNews, index)=>(
                        <div key={index} className="max-w-sm rounded overflow-hidden shadow-lg">
                            <img src={`https://www.nytimes.com/${imageUrls[index]}`} alt="news"/>
                            <div className="px-6 py-4">
                                <h1 className="text-lg font-semibold text-center text-red-400">
                                    {singleNews.headline.main}
                                </h1>
                                <p className="text-md pt-3">
                                    {singleNews.abstract}
                                </p>
                                <ul>
                                    <li>
                                        <h6 className="font-bold text-blue-600">
                                            Published on : {formateDate(singleNews.pub_date)}
                                        </h6>
                                    </li>
                                    <li>
                                        <a href={singleNews.web_url} target="_blank" rel="noopener noreferrer">
                                        <button className="rounded mt-4 hover:bg-red-600 bg-red-400 text-white px-4 py-2">
                                            Read more
                                            <ChevronRightIcon/>
                                        </button>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    ))
                }
        </div>
    )
}

export default News
