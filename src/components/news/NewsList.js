import axios from 'axios'
import React, { useEffect, useState } from 'react'
import CircularProgressBar from '../CircularProgressBar'
import Footer from '../footer/Footer'
import Header from '../header/Header'
import SearchComponent from '../SearchComponent'
import News from './News'

function NewsList() {
    const [news, setNews] = useState([])
    const [isLoading, setIsLoading] = useState(true)

    useEffect(()=>{
        const fetchNews = async () =>{
            const results = await axios.get(
                `https://api.nytimes.com/svc/search/v2/articlesearch.json?q=sports&api-key=${process.env.REACT_APP_NYT_API_KEY}`,
             )
             setNews(results.data.response.docs)
             setIsLoading(false)
        }
        fetchNews()
    },[])



    let newsComponent = null
    if(isLoading){
        newsComponent = <CircularProgressBar/>
    }else{
        newsComponent = <News news={news} />  
    }
    return (
        <React.Fragment>
           <Header/>
           <div className="min-h-full">
                <SearchComponent />
                {newsComponent}
            </div>

            <Footer/>
        </React.Fragment>
    )
}

export default NewsList
