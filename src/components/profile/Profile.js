import React, { useEffect, useState } from 'react'
import Header from '../header/Header'
import {AccountCircle} from '@material-ui/icons'
import CircularProgressBar from '../CircularProgressBar'

import './Profile.css'

function Profile() {

    const [user, setUser] = useState({})
    const [isLoading, setIsLoading] = useState(true)


    useEffect(()=>{
        setUser(getUserFromToken(localStorage.getItem('token')))
        setIsLoading(false)
    },[setUser])
        

    const getUserFromToken = token => {
        if (token) {
          try {
            return JSON.parse(atob(token.split('.')[1]));
          } catch (error) {
          }
        }
      
        return null;
      }


    const formateDate = (rowDate) =>{
        let date = new Date(rowDate)
        return date.toDateString()
    }

    return (
        <React.Fragment>
            <Header />
            {
                isLoading ? <CircularProgressBar /> :
                <div>
                    <div className="profile-container text-center py-10 mx-auto mt-10 bg-gray-300 w-28">
                        <AccountCircle fontSize="large" />
                    </div>
                    <h1 className="text-2xl mt-4 text-center font-semibold">Email: {user.user.email}</h1>
                    <h1 className="text-xl text-center font-semibold">Name: {user.user.name}</h1>
                    <h1 className="text-xl text-center font-semibold">Joined on: {formateDate(user.user.date)}</h1>
            </div>
            }
            
        </React.Fragment>
    )
}

export default Profile
