import React, {useState, useEffect} from 'react'
import League from './League'
import Header from "../header/Header"
import API from '../../api'
import CircularProgressBar from '../CircularProgressBar'

function LeagueList() {

    const [leagues, setLeagues] = useState([])
    const [isLoading, setIsLoading] = useState(true)

    useEffect(()=>{
        fetchLeagues()
    },[])

    const fetchLeagues = async () =>{
        const results = await API.get('leagues')
        setLeagues(results.data.leagues)
        console.log(results.data.leagues)
        setIsLoading(false)
    }

    return (
        <React.Fragment>
        <Header/>
        {
            isLoading? <CircularProgressBar />:
            <div>
                {
                    leagues.map((league, index)=>(
                        <League key={index} league = {league} />
                    ))
                }
            </div>
        }
        </React.Fragment>
    )
}

export default LeagueList
