import React from 'react'
import { Tab } from 'semantic-ui-react'

const panes = [
  {
    menuItem: 'Fixtures',
    render: () => <Tab.Pane loading>Tab 1 Content</Tab.Pane>,
  },
  { menuItem: 'Teams', render: () => <Tab.Pane>
    <h3>Shujaa 7s men</h3>
    <h3>Shujaa 15s women</h3>
  </Tab.Pane> },
  { menuItem: 'Players', render: () => <Tab.Pane>
    
    <h3 className="text-xl">Mike Alvin Ochieng</h3>
    <h3 className="text-xl">Austine Mwangi</h3>
    <h3 className="text-xl">Muhammed Abdul</h3>
  </Tab.Pane> },
]


function LeagueTabs() {      
        return (
          <div>
            <Tab panes={panes} />
          </div>
        )

}

export default LeagueTabs
