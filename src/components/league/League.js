import React from 'react'
// import { Link } from 'react-router-dom'

function League({league}) {

    const formatDate = (rowDate) =>{
        let date = new Date(rowDate)
        return date.toDateString()
    }

    return (
        <React.Fragment>
            {/* <Link to={`/leagues/${league._id}`}> */}
                <div className="w-1/2 mx-auto mt-3">
                    <div className="rounded shadow-lg p-7">
                        <h1 className="text-center text-2xl font-bold" >{league.name}</h1>
                        <h1 className="text-xl font-bold">Teams</h1>
                        {league.teams.map((team, index) =>(
                            <h2 key={index}>{team}</h2>
                        ))}
                        <h5 className="float-right text-red-500">Started on: {formatDate(league.startDate)}</h5>
                    </div>
                </div>
            {/* </Link> */}
        </React.Fragment>
    )
}

export default League
