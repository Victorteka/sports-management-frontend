import React from 'react'
import Header from '../header/Header'
import LeagueTabs from './LeagueTabs'
import './League.css'


function LeagueDetails() {
    // const backgroundImage = {
    //     backgroundImage: `url(${rubyPhoto}) ,linear-gradient(rgba(0,0,0,0.5),rgba(0,0,0,0.5))`,
    //     height: '50vh',
    //     positon: 'center',
    //     backgroundSize: 'cover',
    //     backgroundRepeat: 'no-repeat'
    // }

    return (
        <React.Fragment>
            <Header />
            <div className="backgroundImage">
                <div className="flex flex-wrap justify-center ">
                    <button className="px-12 hover:bg-red-500 py-3 mt-48 bg-red-400 rounded-3xl text-white">Join</button>
                </div>
            </div>
            <LeagueTabs />
        </React.Fragment>
    )
}

export default LeagueDetails
