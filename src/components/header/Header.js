import React from 'react'
import { Link} from 'react-router-dom'
import {AccountCircle} from '@material-ui/icons'
import {ArrowDropDown} from '@material-ui/icons';

import './Header.css'

function Header() {
    
    const token = localStorage.getItem('token')

    let navbarComponent = null

    if(token !== null){
        navbarComponent = <div className="py-4 px-6 flex justify-between content-center shadow-xl">
        <h1 className="text-3xl font-semibold text-red-500 cursor-pointer hover:text-red-600">
            <Link to='/'>Sport Centre</Link>
        </h1>
        <div className="flex flex-row content-center">
            <h2 className="text-md font-semibold mr-8 pt-2">
                <Link to='/'>Home</Link>
            </h2>
            <h2 className="text-md font-semibold mr-8 pt-2">
                <Link to='/sports'>Sports</Link>
            </h2>
            <h2 className="text-md font-semibold mr-8 pt-2">
                <Link to='/tournaments'>Tournaments</Link>
            </h2>
            <h2 className="text-md font-semibold mr-8 pt-2">
                <Link to='/leagues'>Leagues</Link>
            </h2>
            <h2 className="text-md font-semibold mr-8 pt-2">
                <Link to='/news'>News</Link>
            </h2>
            <div className="pt-2 mr-20 dropdown">
                <AccountCircle/>
                <ArrowDropDown/>
                <ul class="dropdown-menu absolute hidden text-gray-700 pt-1">
                    <li><Link to="/profile" className="rounded-t bg-gray-200 hover:bg-gray-400 py-2 px-12 block whitespace-no-wrap">Profile</Link></li>
                    {/* <li><Link className="bg-gray-200 hover:bg-gray-400 py-2 px-4 block whitespace-no-wrap">Two</Link></li> */}
                    <li><Link to='/logout' className="rounded-b bg-gray-200 hover:bg-gray-400 py-2 px-12 block whitespace-no-wrap">Logout</Link></li>
                </ul>
            </div>
        </div>
    </div>
    }else{
        navbarComponent = navbarComponent = <div className="py-4 px-6 flex flex-wrap justify-between content-center shadow-xl">
        <h1 className="text-3xl font-semibold text-red-500 cursor-pointer hover:text-red-600">
            <Link to='/'>Sport Centre</Link>
        </h1>
        <div className="flex flex-row content-center">
            <h2 className="text-md font-semibold mr-8 pt-2">
                <Link to='/'>Home</Link>
            </h2>
            <h2 className="text-md font-semibold mr-8 pt-2">
                <Link to='/sports'>Sports</Link>
            </h2>
            <h2 className="text-md font-semibold mr-8 pt-2">
                <Link to='/tournaments'>Tournaments</Link>
            </h2>
            <h2 className="text-md font-semibold mr-8 pt-2">
                <Link to='/leagues'>Leagues</Link>
            </h2>
            <h2 className="text-md font-semibold mr-16 pt-2">
                <Link to='/news'>News</Link>
            </h2>
            <div className="bg-red-400 text-center rounded-3xl text-white mr-3 hover:bg-red-600 cursor-pointer">
                <h2 className="text-md font-semibold py-2 px-4">
                    <Link to='/login'>Log in</Link>
                </h2>
            </div>
            <div className="bg-gray-200 text-center rounded-3xl mr-3 hover:bg-gray-300 cursor-pointer">
                <h2 className="text-md font-semibold py-2 px-3">
                    <Link to='/register'>Sign up</Link>
                </h2>
            </div>
        </div>
    </div>
    }

    return (
            <React.Fragment>
                {navbarComponent}
            </React.Fragment>
    )
}

export default Header
