import React from 'react'
import Header from '../header/Header'
import LeagueTabs from '../league/LeagueTabs'
import './Sport.css'

function SportsDetails() {
    return (
        <React.Fragment>
            <Header />
            <div className="backgroundImage">
                <div className="flex flex-wrap justify-center ">
                    <button className="px-12 hover:bg-red-500 py-3 mt-48 bg-red-400 rounded-3xl text-white">Join</button>
                </div>
            </div>
            <LeagueTabs />
        </React.Fragment>
    )
}

export default SportsDetails
