import React, {useState, useEffect} from 'react'
import Header from '../header/Header'
import API from '../../api'
import Sport from './Sport'
import CircularProgressBar from '../CircularProgressBar'

function SportsList() {

    const [sports, setSports] = useState([])
    const [isLoading, setIsLoading] = useState(true)

    useEffect(()=>{
        fetchSports()
    },[])

    const fetchSports = async () =>{
        const results = await API.get('sports')
        setSports(results.data.sports)
        console.log(results.data.sports)
        setIsLoading(false)
    }

    return (
        <React.Fragment>
            <Header/>
           <div>
           {isLoading?<CircularProgressBar/>
            :<div>
                {
                    sports.map((sport, index) =>(
                        <Sport key={index} sport={sport} />
                    ))
                }
            </div>}
           </div>
        </React.Fragment>
    )
}

export default SportsList
