import React from 'react'
import { Link } from 'react-router-dom'
import ChevronRightIcon from '@material-ui/icons/ChevronRight';

function Sport({sport}) {
    return (
        <div>
            <div className="w-1/2 mx-auto mt-3">
                <div className="rounded shadow-lg p-4">
                    <h1 className="text-center text-2xl" >{sport.name}</h1>
                    <span className="text-lg font-bold">Coach:</span> <span>{sport.coach}</span><br/>
                    <span className="text-lg font-bold">Captain:</span> <span>{sport.captain}</span> <br />
                    <button className="rounded mt-4 hover:bg-red-600 bg-red-400 text-white px-4 py-2">
                        <Link to={`/sports/${sport._id}`} >More details</Link>
                    <ChevronRightIcon/>
                    </button>
                </div>
                
            </div>
        </div>
    )
}

export default Sport
